import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Assignment2 {
	
	private static List<Event> events;
	private static Set<String> keys;
	static {
		events = new ArrayList<>(11);
		keys = new LinkedHashSet<>();
//		keys.add("k1");
//		keys.add("k3");
//		keys.add("k4");
//		keys.add("k5");
//		keys.add("k2");
//		keys.add("k6");
		
		events.add(new Event(0, "k1", "v1"));
		events.add(new Event(1, "k2", "v2"));
		events.add(new Event(2, "k1", "v3"));
		events.add(new Event(3, "k1", "v4"));
		events.add(new Event(4, "k3", "v5"));
		events.add(new Event(5, "k2", "v6"));
		events.add(new Event(6, "k4", "v7"));
		events.add(new Event(7, "k5", "v8"));
		events.add(new Event(8, "k5", "v9"));
		events.add(new Event(9, "k2", "v10"));
		events.add(new Event(10, "k6", "v11"));
		keys = new LinkedHashSet<>(events.stream().map(event -> event.getKey()).collect(Collectors.toList()));
	}
	
	public static void main(String[] args) {
		for(String key:keys) {
			Event event = new Event(key);
			List<Event> currentEvents = new ArrayList<>(events);
			currentEvents.retainAll(Arrays.asList(event));
			events.removeAll(currentEvents);
			events.add(Collections.max(currentEvents));
		}
		System.out.println(events.toString().replace("[", "").replace("]", ""));
	}
	
	static class Event implements Comparable<Event>{
		private Integer eventId;
		private String key;
		private String value;

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Event other = (Event) obj;
			if (key == null) {
				if (other.key != null)
					return false;
			} else if (!key.equals(other.key))
				return false;
			return true;
		}

		public Event(String key) {
			super();
			this.key = key;
		}
		
		public Event(int eventId, String key, String value) {
			super();
			this.eventId = eventId;
			this.key = key;
			this.value = value;
		}
		public int getEventId() {
			return eventId;
		}
		public void setEventId(int eventId) {
			this.eventId = eventId;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		@Override
		public String toString() {
			return "(" + eventId + ", " + key + ", " + value + ")";
		}
		@Override
		public int compareTo(Event o) {
			return this.eventId.compareTo(o.eventId);
		}
		
		
	}

}
